﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Linq;

namespace _3DMASample
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string dir = Directory.GetCurrentDirectory();
            try
            {
                int retVal = PreProcessor.PerformPreprocessing(@"./Assets/1679_Original.fda");
                if (retVal == 1)
                {
                    Console.WriteLine("Success");
                }
                else
                {
                    Console.Write("Failed");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Hit Enter to exit...");
                Console.ReadLine();
            }

        }
    }

    public enum Flag
    {
        CPU,
        GPU,
        GPU_TEMP,
        GPU_SINGLE_OP1,
        GPU_SINGLE_OP2,
        GPU_SINGLE_OP3,
        NoTopQ_GPU = 100
    }

    public enum retVal
    {
        SUCCESS1,
        NULL_PTR,
        INVALID_IMG_SIZE,
        INVALID_FLAG_VAL,
        INVALID_IFRAME
    };


    internal class JaggedArrayMarshaler : ICustomMarshaler
    {
        private static ICustomMarshaler GetInstance(string cookie)
        {
            return new JaggedArrayMarshaler();
        }

        private GCHandle[] handles;
        private GCHandle buffer;
        private Array[] array;
        public void CleanUpManagedData(object ManagedObj)
        {
        }
        public void CleanUpNativeData(IntPtr pNativeData)
        {
            buffer.Free();
            foreach (GCHandle handle in handles)
            {
                handle.Free();
            }
        }
        public int GetNativeDataSize()
        {
            return 4;
        }
        public IntPtr MarshalManagedToNative(object ManagedObj)
        {
            array = (Array[])ManagedObj;
            handles = new GCHandle[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                handles[i] = GCHandle.Alloc(array[i], GCHandleType.Pinned);
            }
            var pointers = new IntPtr[handles.Length];
            for (int i = 0; i < handles.Length; i++)
            {
                pointers[i] = handles[i].AddrOfPinnedObject();
            }
            buffer = GCHandle.Alloc(pointers, GCHandleType.Pinned);
            return buffer.AddrOfPinnedObject();
        }
        public object MarshalNativeToManaged(IntPtr pNativeData)
        {
            return array;
        }
    }

    public static unsafe class MovingAverageWrapper
    {
        [DllImport("Improv3DMovAvg.dll")]
        public static extern bool LoadDll();

        [DllImport("Improv3DMovAvg.dll")]
        public static extern void initImprov3DMovAvgParams(int optFlowNumIters, int spatialFilterStrength, int spatialWindowSize, int spatialWeights);

        [DllImport("Improv3DMovAvg.dll")]
        public static extern retVal runImprov3DMovAvg_WholeVol_32Bit(float[][] inputImageBuffer3D, float[][] outputMovAvgBuffer3D, float globalMin, float globalMax, float blendThresh, int nFrames, int nHeight, int nWidth, int flag);

        [DllImport("Improv3DMovAvg.dll")]
        public static extern retVal runImprov3DMovAvg_WholeVol_16Bit(float** inputImageBuffer3D, ushort** outputMovAvgBuffer3D, ref float globalMin, ref float globalMax, ref float blendThresh, int nFrames, int nHeight, int nWidth, float[] topQ, int flag);

        [DllImport("Improv3DMovAvg.dll")]
        public static extern retVal runImprov3DMovAvg_PreCalc(float[][] inputImageBuffer3D, float globalMin, float globalMax, int nFrames, int nHeight, int nWidth, int flag);

        [DllImport("Improv3DMovAvg.dll")]
        public static extern retVal runImprov3DMovAvg_SingleImg_8Bit(int indx, byte[] outputImg, float globalMin, float globalMax, float blendThresh, int nHeight, int nWidth, float topQ, int flag);

        [DllImport("Improv3DMovAvg.dll")]
        public static extern retVal runImprov3DMovAvg_SingleImg_16Bit(int indx, ushort[] outputImg, float globalMin, float globalMax, float blendThresh, int nHeight, int nWidth, float topQ, int flag);

    }

    public static class PreProcessor
    {
        public static readonly int DIM_SIZE = 64;
        public static readonly bool B_FLIP = false;
        public static readonly int NEW_MIN = 0;
        public static readonly int NEW_MAX = 65535;

        public static GCHandle[] PinMap<T>(T[][] map)
        {
            var handles = new GCHandle[map.Length];
            for (var y = 0; y < map.Length; y += 1)
            {
                handles[y] = GCHandle.Alloc(map[y], GCHandleType.Pinned);
            }
            return handles;
        }

        public static unsafe GCHandle PtrMap(GCHandle[] map)
        {
            var ptrs = new IntPtr[map.Length];
            for (var i = 0; i < ptrs.Length; i += 1)
            {
                ptrs[i] = map[i].AddrOfPinnedObject();
            }
            return GCHandle.Alloc(ptrs, GCHandleType.Pinned);
        }

        public static int PerformPreprocessing(string filePath)
        {
            try
            {
                var fda = Topcon.Fda.FromFile(filePath);
                float[] topQ = new float[6];
                if (fda.FastQ2Info != null)
                {
                    LoadFastQ(topQ, fda.FastQ2Info);
                    if (topQ[1] > 1000.0f)
                    {
                        AssignDefaultValues(topQ);
                    }
                }
                else
                {
                    throw new Exception("No FastQ info found in fda file. All 3d scans are supposed to have this information.");
                }

                //check if bscans exist 
                Topcon.Objects.ImgJpeg bscans = fda.ImgJpeg;
                if (bscans == null)
                {
                    throw new Exception("Could not find bscans. Please verify the fda file used.");
                }
                uint nFrame = bscans.ImageCount;
                ushort[][] inputVol = new ushort[nFrame][];
                float[][] preTopQVol = new float[nFrame][];
                ushort[][] outputBuffer = new ushort[nFrame][];
                int nWidth = (int)bscans.ImageWidth;
                int nHeight = (int)bscans.ImageHeight;


                for (uint iFrame = 0; iFrame < nFrame; ++iFrame)
                {
                    inputVol[iFrame] = new ushort[nWidth * nHeight];
                    preTopQVol[iFrame] = new float[nWidth * nHeight];
                    outputBuffer[iFrame] = new ushort[nWidth * nHeight];
                    // Uncompress the data since input is fda
                    UncompressImg(bscans.Images[iFrame].GetBytes(), inputVol[iFrame], nWidth, nHeight);
                    InverseTransposeTopQ(inputVol[iFrame], preTopQVol[iFrame], nWidth, nHeight, topQ);
                    TransposeImage(preTopQVol[iFrame], nWidth, nHeight);
                }

                unsafe
                {

                    var pinTopQ = PinMap(preTopQVol);
                    var pinInput = PinMap(inputVol);
                    var pinOutput = PinMap(outputBuffer);
                    var ptrTopQ = PtrMap(pinTopQ);
                    var ptrInput = PtrMap(pinInput);
                    var ptrOutput = PtrMap(pinOutput);
                    float globalMin = 0f;
                    float globalMax = 0f;
                    float blendThresh = 0.70f;
                    Console.WriteLine($"starting moving average");
                    var result = MovingAverageWrapper.runImprov3DMovAvg_WholeVol_16Bit((float**)ptrInput.AddrOfPinnedObject(), (ushort**)ptrOutput.AddrOfPinnedObject().ToPointer(), ref globalMin, ref globalMax, ref blendThresh, (int)nFrame, nHeight, nWidth, topQ, 0);
                    Console.WriteLine($"moving average result: {result}");

                }

                return 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to perform preprocessing! Exception : {ex.Message}");
                return -1;
            }

        }


        public static void LoadFastQ(float[] topQ, Topcon.Objects.FastQ2Info fastQ)
        {
            topQ[0] = fastQ.AbsMin;
            topQ[1] = fastQ.Nf;
            topQ[2] = fastQ.Max;
            topQ[3] = fastQ.AbsMax;
            topQ[4] = fastQ.Qmean;
            topQ[5] = fastQ.Zmean;
        }

        public static void UncompressImg(byte[] slice, ushort[] inputImg, int nWidth, int nHeight)
        {
            if (slice != null)
            {
                RunGaso(slice, inputImg, nWidth, nHeight);
            }

        }
        public static void RunGaso(byte[] slice, ushort[] frame, int width, int height)
        {
            if (slice == null)
            {
                return;
            }

            // Apply the Gaso
            int p1 = 0;
            // unsigned char* p1 = slice;
            for (int k = height - 1; k >= 0; k--)
            {
                int row = (B_FLIP ? k : (height - 1 - k));

                // Reverse order
                int p2 = width * row;
                int p2End = p2 + width;
                for (; p2 < p2End;)
                {
                    // NEW_MAX - NEW_MIN = 65536
                    // +1 = 65537
                    // / 256.0 = 256
                    // * 200 = 51200
                    // + 0 = 51200

                    ushort gaso = (ushort)(((NEW_MAX - NEW_MIN + 1) / 256.0) * slice[p1++] + NEW_MIN);
                    if (gaso < 256)
                    {
                        gaso = 256;
                    }
                    frame[p2++] = gaso;
                }
            }
        }

        public static void TransposeImage(float[] transposeImg, int nWidth, int nHeight)
        {
            float[] pSrcImg = new float[nHeight * nWidth];
            for (int i = 0; i < nHeight * nWidth; i++)
            {
                pSrcImg[i] = transposeImg[i];
            }

            for (int nLineNum = 0; nLineNum < nWidth; nLineNum++)
            {
                int offsetBase = nLineNum * nHeight;
                for (int nRowNum = 0; nRowNum < nHeight; nRowNum++)
                {
                    int offset1 = offsetBase + nRowNum;
                    int offset2 = (nRowNum * nWidth) + nLineNum;

                    transposeImg[offset2] = pSrcImg[offset1];
                }
            }
        }

        public static void InverseTransposeTopQ(ushort[] normalizedImage, float[] preTopQImage, int nWidth, int nHeight, float[] TopQ)
        {
            float fBase0 = TopQ[0];
            float fBase1 = TopQ[1];
            float fBase3 = TopQ[2];
            float fRange01 = ((TopQ[1] - TopQ[0]) / 16383.0F);
            float fRange13 = ((TopQ[2] - TopQ[1]) / 32768.0F);
            float fRange34 = ((TopQ[3] - TopQ[2]) / 16384.0F);

            for (int h = 0; h < nHeight; h++)
            {
                for (int w = 0; w < nWidth; w++)
                {

                    ushort uVal = normalizedImage[(h * nWidth) + w];

                    if (uVal <= 16383U)
                    {
                        // Convert between ABSMIN and NF: 
                        preTopQImage[(w * nHeight) + h] = (fBase0 + ((uVal - 0U) * fRange01));
                    }
                    else if (uVal <= 49151U)
                    {
                        // Convert between NF and MAX: 
                        preTopQImage[(w * nHeight) + h] = (fBase1 + ((uVal - 16383U) * fRange13));
                    }
                    else
                    {
                        // Convert between MAX and ABSMAX: 
                        preTopQImage[(w * nHeight) + h] = (fBase3 + ((uVal - 49151U) * fRange34));
                    }
                }
            }
        }

        public static void AssignDefaultValues(float[] topQ)
        {
            topQ[0] = 0.0F;
            topQ[1] = 35.0F;
            topQ[2] = 70.0F;
            topQ[3] = 75.0F;
        }

    }
}
